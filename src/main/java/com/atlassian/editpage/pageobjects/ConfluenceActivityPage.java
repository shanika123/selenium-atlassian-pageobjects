package com.atlassian.editpage.pageobjects;


import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.atlassian.baseclass.BaseClass;

public class ConfluenceActivityPage extends BaseClass
{
	
		public ConfluenceActivityPage(WebDriver uiDriver) 
		{
			super.webDriver = uiDriver;
		}
		
		private By confluenceLinkActivityLocator = By.xpath("//div[@id='content-body']/div/div/div[2]/div/ul/li/div[2]/ul/li/div[2]/div/a");
		
		/**
		 * Method for Click on ActivityLink 
		 * @return ConfluencePage
		 */
		public ConfluencePage ClickonConfluence()
		{
			
			try
			{
				webDriver.findElement(confluenceLinkActivityLocator).click();
			}
			catch(Exception ex)
			{
				System.out.println(ex + "Error when Clicking on Confluence Activity Page Link");
			}
			return new ConfluencePage(webDriver);
			
		}
	
	

}
