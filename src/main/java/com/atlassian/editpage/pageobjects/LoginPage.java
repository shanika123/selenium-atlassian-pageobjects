package com.atlassian.editpage.pageobjects;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.atlassian.baseclass.BaseClass;

public class LoginPage extends BaseClass
{
	
		public LoginPage(WebDriver uiDriver)
		{
			super.webDriver = uiDriver;
		}
		
		
		private By usernameLocator = By.id("username");
		private By continuebtnLocator = By.xpath("//button[@id='login-submit']/span/span");
		private By passwordLocator = By.xpath("//form[@id='form-login']/div[2]/div/div/div/div/div/div/input");
		private By loginbtnLocator = By.xpath("//button[@id='login-submit']/span/span/span");
		
		
		/**
		 * Method to Enter the User name
		 * @param username
		 * @return LoginPage
		 */
		public LoginPage typeUsername(String username)
		{
			try {
				webDriver.findElement(usernameLocator).sendKeys(username);
			}
			catch(Exception ex)
			{
				System.out.println(ex+"Error in typing username");
			}
			return this;
			
		}
		
		
		/**
		 * Method to Enter the password
		 * @param password
		 * @return LoginPage
		 */
		public LoginPage typePassword(String password)
		{
			try {
				
				webDriver.findElement(passwordLocator).sendKeys(password);
			}
			catch(Exception ex)
			{
				System.out.println(ex+"Error in typing password");
			}
			return this;
			
		}
		
		/**
		 * Method to Click onContinue Button
		 * @return LoginPage
		 */
		public LoginPage coninueLogin()
		{
			try {
				webDriver.findElement(continuebtnLocator).click();
				webDriver.manage().timeouts().implicitlyWait(20,TimeUnit.SECONDS);
			}
			catch(Exception ex)
			{
				System.out.println(ex+"Error in when clicking on Continue Button");
			}
			
			return this;
		}
		
		
		/**
		 * Method to Click on Submit Button
		 * @return LoginPage
		 */
		public LoginPage sumbitLogin()
		{
			try {
				webDriver.findElement(loginbtnLocator).click();
			}
			catch(Exception ex)
			{
				System.out.println(ex+"Error in when clicking on Login Button");
			}
			return this;
			
		}
		
		/**
		 * Method to Login to the Atlassian Home Page
		 * @param username
		 * @param password
		 * @return HomePage
		 */
		public HomePage LoginAs(String username,String password)
		{
			typeUsername(username);
			coninueLogin();
			typePassword(password);
			sumbitLogin();
			webDriver.manage().timeouts().implicitlyWait(600,TimeUnit.SECONDS);
			return new HomePage(webDriver);
			
		}
   
}
