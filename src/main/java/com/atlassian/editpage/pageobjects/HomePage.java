package com.atlassian.editpage.pageobjects;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.atlassian.baseclass.BaseClass;

public class HomePage extends BaseClass{
	
		public HomePage(WebDriver uiDriver) 
		{
			super.webDriver = uiDriver;
		}
		
		private By confluencelinkLocator = By.xpath("//div[@id='root']/div[3]/div/div[2]/div/div/div/div/div[3]/div/div[2]/a/div");
		
		
		/**
		 * Method to Click on Product Title on Home Page
		 * @return ConfluenceActivityPage
		 */
		public ConfluenceActivityPage ClickonConfluenceProductTile()
		{
			try
			{
				webDriver.findElement(confluencelinkLocator).isDisplayed();
				webDriver.findElement(confluencelinkLocator).click();
				webDriver.manage().timeouts().implicitlyWait(100,TimeUnit.SECONDS);
			}
			catch(Exception ex)
			{
				System.out.println(ex + "Error when Clicking on Confluence Product Tile");
			}
			return new ConfluenceActivityPage(webDriver);
		}

}
