package com.atlassian.editpage.pageobjects;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.atlassian.baseclass.BaseClass;

public class ConfluencePage extends BaseClass
{
		public ConfluencePage(WebDriver uiDriver) 
		{
			super.webDriver = uiDriver;
		}
		
		private By confluenceEditbuttonLocator = By.xpath("//button[@id='editPageLink']/span/span/span");
		
		/**
		 * Method to Click on Edit Page
		 * @return ConfluenceEditPage
		 */
		public ConfluenceEditPage ClickonEditPage()
		{
			try
			{
				webDriver.findElement(confluenceEditbuttonLocator).click();
				webDriver.manage().timeouts().implicitlyWait(800,TimeUnit.SECONDS);
			}
			catch(Exception ex)
			{
				System.out.println(ex + "Error when clicking on Confluence Edit Icon");
			}
			return new ConfluenceEditPage(webDriver);
		}
	
}
