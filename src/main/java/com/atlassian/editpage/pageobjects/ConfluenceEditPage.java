package com.atlassian.editpage.pageobjects;

import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.Select;

import com.atlassian.baseclass.BaseClass;


public class ConfluenceEditPage extends BaseClass
{
	public ConfluenceEditPage(WebDriver uiDriver) 
	{
		super.webDriver = uiDriver;
	}
 
	
	private By restrictionsLocator = By.xpath("//div[@id='editor-precursor-buttons']/div/span/div/button/span/span/img");
	private By restrictionDropDownLocator = By.xpath("//body[@id='com-atlassian-confluence']/div[2]/div[2]/div/div[3]/div[2]/div/div/div/div/div/div/div/div/div/div[2]/div/span");
	private By searchboxLocator = By.xpath("//body[@id='com-atlassian-confluence']/div[2]/div[2]/div/div[3]/div[2]/div/div/div/div/div/div[2]/div/div/div/div/div/div");
	private By searchNameLocator = By.xpath("//body[@id='com-atlassian-confluence']/div[2]/div[2]/div/div[3]/div[2]/div/div/div/div/div/div[2]/div/div/div/div/div/div/div");
	private By userselection = By.xpath("//div[@id='react-select-3-option-0']/div/div[2]");
	private By changePermissiondropboxLocator = By.xpath("//body[@id='com-atlassian-confluence']/div[2]/div[2]/div/div[3]/div[2]/div/div/div/div/div/div[2]/div/div/div[2]/div/div");
	private By canViewPermissonLocator = By.xpath("//div[@id='react-select-4-option-0']");
	private By addUserLocator = By.xpath("//body[@id='com-atlassian-confluence']/div[2]/div[2]/div/div[3]/div[2]/div/div/div/div/div/div[2]/div/div/div[3]/button/span/span");
	private By applybtnLocator = By.xpath("//body[@id='com-atlassian-confluence']/div[2]/div[2]/div/div[3]/div[2]/div/div/footer/div/div/button/span/span");
	private By removeUserLocator = By.xpath("//body[@id='com-atlassian-confluence']/div[2]/div[2]/div/div[3]/div[2]/div/div/div/div/div/div[3]/div/table/tbody/tr[3]/td[3]/button/span/span/span");
	
	
		/**
		 * Method to click on Restrictions Icon
		 * @return ConfluenceEditPage
		 */
		public ConfluenceEditPage ClickonRestrictionsIcon()
		{
			try
			{
				webDriver.findElement(restrictionsLocator).click();
			}
			catch(Exception ex)
			{
				System.out.println(ex + "Error in Clicking on Restrictions Icon");
			}
			return this;
		}
		
		
		/**
		 * Method to click on Set the Restrictions
		 * @return ConfluenceEditPage
		 */
		public ConfluenceEditPage SetRestriction()
		{
			try
			{
				webDriver.findElement(restrictionDropDownLocator).click();
				Select selectOption = new Select(webDriver.findElement(restrictionDropDownLocator));
				selectOption.selectByVisibleText("Viewing and editing restricted");
				
			}
			catch(Exception ex)
			{
				System.out.println(ex + "Error in Setting the Restriction to View and Edit");
			}
			return this;
			
		}
		
		/**
		 * Method to Search the User
		 * @param username
		 * @return ConfluenceEditPage
		 */
		
		public ConfluenceEditPage SearchUser(String username)
		{
			try
			{
				webDriver.findElement(searchboxLocator).click();
				webDriver.findElement(searchNameLocator).sendKeys(username);
				webDriver.manage().timeouts().implicitlyWait(600,TimeUnit.SECONDS);
				 
			}
			catch(Exception ex)
			{
				System.out.println(ex + "Error when searching the user");
			}
			return this;
		}
		
		
		/**
		 * Method to Select the User
		 * @return ConfluenceEditPage
		 */
		public ConfluenceEditPage SelectUser()
		{
			try 
			{
				webDriver.findElement(userselection).click();
				
			}
			catch(Exception ex)
			{
				System.out.println(ex + "Error when Selecting the User");
			}
			return this;
		}
		
		
		/**
		 * Method to Edit the Permissions of the User
		 * @return ConfluenceEditPage
		 */
		public ConfluenceEditPage EditPermission()
		{
			try
			{
				webDriver.findElement(changePermissiondropboxLocator).click();
				webDriver.findElement(canViewPermissonLocator).click();
			}
			catch(Exception ex)
			{
				System.out.println(ex + "Error when Editing the User Permission of Dropbox");
			}
			
			return this;
		}
		
		/**
		 * Method to Add the User
		 * @return ConfluenceEditPage
		 */
		public ConfluenceEditPage AddUser()
		{
			try
			{
				webDriver.findElement(addUserLocator).click();			
			}
			catch(Exception ex)
			{
				System.out.println("Error when Adding the User - Click on the Add User button");
			}
			return this;
			
		}
		
		/**
		 * Method to Apply the User
		 * @return ConfluenceEditPage
		 */
		
		public ConfluenceEditPage ApplyUser()
		{
			try
			{
				webDriver.findElement(applybtnLocator).click();
			}
			catch(Exception ex)
			{
				System.out.println("Error when Applying the User - Click on the Apply User button");
			}
			return this;
		}
		
		
		/**
		 * Method to Remove the User
		 * @return ConfluenceEditPage
		 */
		public ConfluenceEditPage RemoveUser()
		{
			try
			{
				webDriver.findElement(removeUserLocator).click();
			}
			catch(Exception ex)
			{
				System.out.println("Error when Removing the User - Click on the Remove User button");
			}
			return this;
			
		}
}
