
## Automation Technique

	Page Object Model has been used in design for the automation of test scenarios to represent a 	better logical relationship between  the pages of application.
#Page Object model 
	-Reduces the duplication of code
	-Makes tests more readable and robust
	-Improves the maintainability
#Assertions 
	has been  implemented in order to validate certain areas.
#Implicit waits 
	have been used for places as some elements need some time to get located.
#Page Objects Repository
	1.A separate Page Objects repository can make the project more maintainable.
	2.If the same page objects are applicable for another test repo, this can be reused as a 	dependency. Developers can also use the same Page Objects repository to proceed with their 	tests.
	3.Selenium framework has been used as a dependency in pom.xml
	A sub package has been implemented for the Base Class to initialize the web driver instance.
